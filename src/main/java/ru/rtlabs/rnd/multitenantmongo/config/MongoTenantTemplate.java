/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 *
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.rtlabs.rnd.multitenantmongo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.MongoClientFactory;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.CollectionCallback;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.DbCallback;
import org.springframework.data.mongodb.core.DocumentCallbackHandler;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.ScriptOperations;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.index.IndexOperations;
import org.springframework.data.mongodb.core.mapreduce.GroupBy;
import org.springframework.data.mongodb.core.mapreduce.GroupByResults;
import org.springframework.data.mongodb.core.mapreduce.MapReduceOptions;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.CloseableIterator;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;
import ru.rtlabs.rnd.multitenantmongo.config.props.DataSourceProperties;
import ru.rtlabs.rnd.multitenantmongo.dto.UserDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component("mongoTemplate")
@EnableConfigurationProperties(DataSourceProperties.class)
public class MongoTenantTemplate extends MongoTemplate {
    private static Map<String, MongoTemplate> tenantTemplates = new HashMap<>();

    private DataSourceProperties dataSourceProperties;
    private ObjectMapper mapper;
    private Environment environment;

    @Autowired
    public MongoTenantTemplate(DataSourceProperties dataSourceProperties, Environment environment, ObjectMapper mapper) {
        super(configureDefaultFactory(dataSourceProperties, environment));
        this.dataSourceProperties = dataSourceProperties;
        this.mapper = mapper;
        this.environment = environment;
        this.configureTenantTemplates();
    }

    @SuppressWarnings("deprecation")
    private void configureTenantTemplates() {
        dataSourceProperties.getDatasources().forEach(ds -> {
            final MongoClient mongoClient = new MongoClientFactory(ds, environment)
                    .createMongoClient(null);
            final MongoTemplate mongoTemplate = new MongoTemplate(new SimpleMongoDbFactory(mongoClient, ds.getDatabase()));
            tenantTemplates.put(ds.getTenantName(), mongoTemplate);
        });
    }

    @SuppressWarnings("deprecation")
    private static MongoDbFactory configureDefaultFactory(DataSourceProperties dataSourceProperties, Environment environment) {
        final MongoProperties mongoProperties = dataSourceProperties.getDatasources().get(0);
        final MongoClient mongoClient =
                new MongoClientFactory(mongoProperties, environment)
                        .createMongoClient(null);
        return new SimpleMongoDbFactory(mongoClient, mongoProperties.getDatabase());
    }

    /**
     * Tenant resolver, implement as you need
     *
     * @return resolved tenant name
     */
    private String resolveCurrentTenantIdentifier() {
        final OAuth2Authentication auth = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        final UserDetails principal;
        if (auth != null) {
            principal = mapper.convertValue(auth.getUserAuthentication().getDetails(), UserDetails.class);
            return principal.getTenantName();
        }
        return dataSourceProperties.getDatasources().get(0).getTenantName();
    }

    protected MongoTemplate getTenantMongoTemplate() {
        final String tenant = this.resolveCurrentTenantIdentifier();
        return tenantTemplates.get(tenant);
    }

    @Override
    public String getCollectionName(Class<?> entityClass) {
        return getTenantMongoTemplate().getCollectionName(entityClass);
    }

    @Override
    public Document executeCommand(String jsonCommand) {
        return getTenantMongoTemplate().executeCommand(jsonCommand);
    }

    @Override
    public void executeQuery(Query query, String collectionName, DocumentCallbackHandler dch) {
        getTenantMongoTemplate().executeQuery(query, collectionName, dch);

    }

    @Override
    public <T> T execute(DbCallback<T> action) {
        return getTenantMongoTemplate().execute(action);
    }

    @Override
    public <T> T execute(Class<?> entityClass, CollectionCallback<T> action) {
        return getTenantMongoTemplate().execute(entityClass, action);
    }

    @Override
    public <T> T execute(String collectionName, CollectionCallback<T> action) {
        return getTenantMongoTemplate().execute(collectionName, action);
    }

    @Override
    public <T> CloseableIterator<T> stream(Query query, Class<T> entityType) {
        return getTenantMongoTemplate().stream(query, entityType);
    }

    @Override
    public <T> MongoCollection<Document> createCollection(Class<T> entityClass) {
        return getTenantMongoTemplate().createCollection(entityClass);
    }

    @Override
    public <T> MongoCollection<Document> createCollection(Class<T> entityClass, CollectionOptions collectionOptions) {
        return getTenantMongoTemplate().createCollection(entityClass, collectionOptions);
    }

    @Override
    public MongoCollection<Document> createCollection(String collectionName) {
        return getTenantMongoTemplate().createCollection(collectionName);
    }

    @Override
    public MongoCollection<Document> createCollection(String collectionName, CollectionOptions collectionOptions) {
        return getTenantMongoTemplate().createCollection(collectionName, collectionOptions);
    }

    @Override
    public Set<String> getCollectionNames() {
        return getTenantMongoTemplate().getCollectionNames();
    }

    @Override
    public MongoCollection<Document> getCollection(String collectionName) {
        return getTenantMongoTemplate().getCollection(collectionName);
    }

    @Override
    public <T> boolean collectionExists(Class<T> entityClass) {
        return getTenantMongoTemplate().collectionExists(entityClass);
    }

    @Override
    public boolean collectionExists(String collectionName) {
        return getTenantMongoTemplate().collectionExists(collectionName);
    }

    @Override
    public <T> void dropCollection(Class<T> entityClass) {
        getTenantMongoTemplate().dropCollection(entityClass);
    }

    @Override
    public void dropCollection(String collectionName) {
        getTenantMongoTemplate().dropCollection(collectionName);

    }

    @Override
    public IndexOperations indexOps(String collectionName) {
        return getTenantMongoTemplate().indexOps(collectionName);
    }

    @Override
    public IndexOperations indexOps(Class<?> entityClass) {
        return getTenantMongoTemplate().indexOps(entityClass);
    }

    @Override
    public ScriptOperations scriptOps() {
        return getTenantMongoTemplate().scriptOps();
    }

    @Override
    public BulkOperations bulkOps(BulkMode mode, String collectionName) {
        return getTenantMongoTemplate().bulkOps(mode, collectionName);
    }

    @Override
    public BulkOperations bulkOps(BulkMode mode, Class<?> entityType) {
        return getTenantMongoTemplate().bulkOps(mode, entityType);
    }

    @Override
    public BulkOperations bulkOps(BulkMode mode, Class<?> entityType, String collectionName) {
        return getTenantMongoTemplate().bulkOps(mode, entityType, collectionName);
    }

    @Override
    public <T> List<T> findAll(Class<T> entityClass) {
        return getTenantMongoTemplate().findAll(entityClass);
    }

    @Override
    public <T> List<T> findAll(Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate().findAll(entityClass, collectionName);
    }

    @Override
    public <T> GroupByResults<T> group(String inputCollectionName, GroupBy groupBy, Class<T> entityClass) {
        return getTenantMongoTemplate().group(inputCollectionName, groupBy, entityClass);
    }

    @Override
    public <T> GroupByResults<T> group(Criteria criteria, String inputCollectionName, GroupBy groupBy,
                                       Class<T> entityClass) {
        return getTenantMongoTemplate().group(criteria, inputCollectionName, groupBy,
                entityClass);
    }

    @Override
    public <O> AggregationResults<O> aggregate(TypedAggregation<?> aggregation, String collectionName,
                                               Class<O> outputType) {
        return getTenantMongoTemplate().aggregate(aggregation, outputType);
    }

    @Override
    public <O> AggregationResults<O> aggregate(TypedAggregation<?> aggregation, Class<O> outputType) {
        return getTenantMongoTemplate().aggregate(aggregation, outputType);
    }

    @Override
    public <O> AggregationResults<O> aggregate(Aggregation aggregation, Class<?> inputType, Class<O> outputType) {
        return getTenantMongoTemplate().aggregate(aggregation, inputType, outputType);
    }

    @Override
    public <O> AggregationResults<O> aggregate(Aggregation aggregation, String collectionName, Class<O> outputType) {
        return getTenantMongoTemplate().aggregate(aggregation, collectionName, outputType);
    }

    @Override
    public <T> MapReduceResults<T> mapReduce(String inputCollectionName, String mapFunction, String reduceFunction,
                                             Class<T> entityClass) {
        return getTenantMongoTemplate().mapReduce(inputCollectionName, mapFunction,
                reduceFunction, entityClass);
    }

    @Override
    public <T> MapReduceResults<T> mapReduce(String inputCollectionName, String mapFunction, String reduceFunction,
                                             MapReduceOptions mapReduceOptions, Class<T> entityClass) {
        return getTenantMongoTemplate().mapReduce(inputCollectionName, mapFunction,
                reduceFunction, mapReduceOptions, entityClass);
    }

    @Override
    public <T> MapReduceResults<T> mapReduce(Query query, String inputCollectionName, String mapFunction,
                                             String reduceFunction, Class<T> entityClass) {
        return getTenantMongoTemplate().mapReduce(query, inputCollectionName, mapFunction,
                reduceFunction, entityClass);
    }

    @Override
    public <T> MapReduceResults<T> mapReduce(Query query, String inputCollectionName, String mapFunction,
                                             String reduceFunction, MapReduceOptions mapReduceOptions, Class<T> entityClass) {
        return mapReduce(query, inputCollectionName, mapFunction, reduceFunction, mapReduceOptions, entityClass);
    }

    @Override
    public <T> GeoResults<T> geoNear(NearQuery near, Class<T> entityClass) {
        return getTenantMongoTemplate().geoNear(near, entityClass);
    }

    @Override
    public <T> GeoResults<T> geoNear(NearQuery near, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate().geoNear(near, entityClass, collectionName);
    }

    @Override
    public <T> T findOne(Query query, Class<T> entityClass) {
        return getTenantMongoTemplate().findOne(query, entityClass);
    }

    @Override
    public <T> T findOne(Query query, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate().findOne(query, entityClass, collectionName);
    }

    @Override
    public boolean exists(Query query, String collectionName) {
        return getTenantMongoTemplate().exists(query, collectionName);
    }

    @Override
    public boolean exists(Query query, Class<?> entityClass) {
        return getTenantMongoTemplate().exists(query, entityClass);
    }

    @Override
    public boolean exists(Query query, Class<?> entityClass, String collectionName) {
        return getTenantMongoTemplate().exists(query, entityClass, collectionName);
    }

    @Override
    public <T> List<T> find(Query query, Class<T> entityClass) {
        return getTenantMongoTemplate().find(query, entityClass);
    }

    @Override
    public <T> List<T> find(Query query, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate().find(query, entityClass, collectionName);
    }

    @Override
    public <T> T findById(Object id, Class<T> entityClass) {
        return getTenantMongoTemplate().findById(id, entityClass);
    }

    @Override
    public <T> T findById(Object id, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate().findById(id, entityClass, collectionName);
    }

    @Override
    public <T> T findAndModify(Query query, Update update, Class<T> entityClass) {
        return getTenantMongoTemplate().findAndModify(query, update, entityClass);
    }

    @Override
    public <T> T findAndModify(Query query, Update update, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate().findAndModify(query, update, entityClass,
                collectionName);
    }

    @Override
    public <T> T findAndModify(Query query, Update update, FindAndModifyOptions options, Class<T> entityClass) {
        return getTenantMongoTemplate().findAndModify(query, update, options, entityClass);
    }

    @Override
    public <T> T findAndModify(Query query, Update update, FindAndModifyOptions options, Class<T> entityClass,
                               String collectionName) {
        return getTenantMongoTemplate().findAndModify(query, update, options, entityClass,
                collectionName);
    }

    @Override
    public <T> T findAndRemove(Query query, Class<T> entityClass) {
        return getTenantMongoTemplate().findAndRemove(query, entityClass);
    }

    @Override
    public <T> T findAndRemove(Query query, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate().findAndRemove(query, entityClass, collectionName);
    }

    @Override
    public long count(Query query, Class<?> entityClass) {
        return getTenantMongoTemplate().count(query, entityClass);
    }

    @Override
    public long count(Query query, String collectionName) {
        return getTenantMongoTemplate().count(query, collectionName);
    }

    @Override
    public long count(Query query, Class<?> entityClass, String collectionName) {
        return getTenantMongoTemplate().count(query, entityClass, collectionName);
    }

    @Override
    public <T> T insert(T objectToSave) {
        return getTenantMongoTemplate().insert(objectToSave);
    }

    @Override
    public <T> T insert(T objectToSave, String collectionName) {
        return getTenantMongoTemplate().insert(objectToSave, collectionName);
    }

    @Override
    public <T> T save(T objectToSave) {
        return getTenantMongoTemplate().save(objectToSave);
    }

    @Override
    public <T> T save(T objectToSave, String collectionName) {
        return getTenantMongoTemplate().save(objectToSave, collectionName);
    }

    @Override
    public UpdateResult upsert(Query query, Update update, Class<?> entityClass) {
        return getTenantMongoTemplate().upsert(query, update, entityClass);
    }

    @Override
    public UpdateResult upsert(Query query, Update update, String collectionName) {
        return getTenantMongoTemplate().upsert(query, update, collectionName);
    }

    @Override
    public UpdateResult upsert(Query query, Update update, Class<?> entityClass, String collectionName) {
        return getTenantMongoTemplate().upsert(query, update, entityClass, collectionName);
    }

    @Override
    public UpdateResult updateFirst(Query query, Update update, Class<?> entityClass) {
        return getTenantMongoTemplate().updateFirst(query, update, entityClass);
    }

    @Override
    public UpdateResult updateFirst(Query query, Update update, String collectionName) {
        return getTenantMongoTemplate().updateFirst(query, update, collectionName);
    }

    @Override
    public UpdateResult updateFirst(Query query, Update update, Class<?> entityClass, String collectionName) {
        return getTenantMongoTemplate().updateFirst(query, update, entityClass,
                collectionName);
    }

    @Override
    public UpdateResult updateMulti(Query query, Update update, Class<?> entityClass) {
        return getTenantMongoTemplate().updateMulti(query, update, entityClass);
    }

    @Override
    public UpdateResult updateMulti(Query query, Update update, String collectionName) {
        return getTenantMongoTemplate().updateMulti(query, update, collectionName);
    }

    @Override
    public UpdateResult updateMulti(Query query, Update update, Class<?> entityClass, String collectionName) {
        return getTenantMongoTemplate().updateMulti(query, update, entityClass,
                collectionName);
    }

    @Override
    public DeleteResult remove(Object object) {
        return getTenantMongoTemplate().remove(object);
    }

    @Override
    public DeleteResult remove(Object object, String collection) {
        return getTenantMongoTemplate().remove(object, collection);
    }

    @Override
    public DeleteResult remove(Query query, Class<?> entityClass) {
        return getTenantMongoTemplate().remove(query, entityClass);
    }

    @Override
    public DeleteResult remove(Query query, Class<?> entityClass, String collectionName) {
        return getTenantMongoTemplate().remove(query, entityClass, collectionName);
    }

    @Override
    public DeleteResult remove(Query query, String collectionName) {
        return getTenantMongoTemplate().remove(query, collectionName);
    }

    @Override
    public <T> List<T> findAllAndRemove(Query query, String collectionName) {
        return getTenantMongoTemplate().findAllAndRemove(query, collectionName);
    }

    @Override
    public <T> List<T> findAllAndRemove(Query query, Class<T> entityClass) {
        return getTenantMongoTemplate().findAllAndRemove(query, entityClass);
    }

    @Override
    public <T> List<T> findAllAndRemove(Query query, Class<T> entityClass, String collectionName) {
        return getTenantMongoTemplate().findAllAndRemove(query, entityClass, collectionName);
    }

    @Override
    public MongoConverter getConverter() {
        return getTenantMongoTemplate().getConverter();
    }

    @Override
    public MongoDatabase getDb() {
        return getTenantMongoTemplate().getDb();
    }

}
