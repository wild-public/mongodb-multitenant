/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 *
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.rtlabs.rnd.multitenantmongo.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rtlabs.rnd.multitenantmongo.dto.ExampleEntity;
import ru.rtlabs.rnd.multitenantmongo.repo.ExampleRepo;

/**
 * @author Vitaliy Borisovsky
 * @since 02.12.2019
 */

@RestController
@RequestMapping("/person")
@RequiredArgsConstructor
public class RstController {

    private final ExampleRepo repo;

    @GetMapping
    public ExampleEntity get() {
        final ExampleEntity entity = new ExampleEntity();
        entity.setCity("city2");
        return repo.save(entity);
    }
}
